resource "aws_vpc" "Demo-VPC" {
  cidr_block      = "10.0.0.0/24"   
 instance_tenancy = "default"
 tags = {
    Name        = "Project012"
#    creator     = "suraj_12042020"
    region      = "eu-central-1"
    environment = "Testing"
   }
}


output "vpc_id" {
  value = aws_vpc.Demo-VPC.id
}


resource "aws_subnet" "Demo-subnet" {
  vpc_id     = aws_vpc.Demo-VPC.id
  cidr_block = "10.0.0.0/25"          
  availability_zone = "eu-central-1a"

  tags = {
     Name        = "Project012"
#    creator     = "suraj_12042020"
    region      = "eu-central-1a"
    environment = "Testing"
  }
}


resource "aws_subnet" "Demo1-subnet" {
  vpc_id     = aws_vpc.Demo-VPC.id
  cidr_block = "10.0.0.128/25"          
  availability_zone = "eu-central-1a"

  tags = {
     Name        = "Project012"
#    creator     = "suraj_12042020"
    region      = "eu-central-1"
    environment = "Testing"
  }
}

